//
//  JKPerson+Test2.m
//  JKAssociated
//
//  Created by 王冲 on 2018/6/30.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "JKPerson+Test2.h"

#import <objc/runtime.h>

@implementation JKPerson (Test2)

// static ： 代表只能在此文件内访问 static char JKWeightKey;
static char JKWeightKey;

-(void)setWeight:(int)weight{
    
    objc_setAssociatedObject(self, &JKWeightKey, @(weight), OBJC_ASSOCIATION_ASSIGN);
}

-(int)weight{
    
    return [objc_getAssociatedObject(self, &JKWeightKey) intValue];
}
@end

