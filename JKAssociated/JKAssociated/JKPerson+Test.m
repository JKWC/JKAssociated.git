//
//  JKPerson+Test.m
//  JKCategory2
//
//  Created by 王冲 on 2018/6/30.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "JKPerson+Test.h"
#import <objc/runtime.h>
@implementation JKPerson (Test)

const void *JKNameKey = &JKNameKey;
const void *JKHeightKey= &JKHeightKey;

-(void)setName:(NSString *)name{
    
    /**
     设置关联对象
     @param object 设置对象: 在这里是 self
     @param key#> <#key#> description#>
     @param value#> 关联值：这里是 name
     @param policy#> 关联策略
     @return
     */
    
    /**  关联策略
     OBJC_ASSOCIATION_ASSIGN = 0,
     OBJC_ASSOCIATION_RETAIN_NONATOMIC = 1
     OBJC_ASSOCIATION_COPY_NONATOMIC = 3,
     OBJC_ASSOCIATION_RETAIN = 01401,
     OBJC_ASSOCIATION_COPY = 01403
     */
    
    objc_setAssociatedObject(self, JKNameKey, name, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(NSString *)name{
    
    return objc_getAssociatedObject(self, JKNameKey);
}

-(void)setHeight:(int)height{
    
    objc_setAssociatedObject(self, JKHeightKey, @(height), OBJC_ASSOCIATION_ASSIGN);
}

-(int)height{
    
    return [objc_getAssociatedObject(self, JKHeightKey) intValue];
}
@end
