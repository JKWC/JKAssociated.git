//
//  JKPerson+Test4.m
//  JKAssociated
//
//  Created by 王冲 on 2018/7/1.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "JKPerson+Test4.h"
#import <objc/runtime.h>
@implementation JKPerson (Test4)

-(void)setDegree:(NSString *)degree{
    
    objc_setAssociatedObject(self, @selector(degree), degree, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

/**
   _cmd在这里等于 @selector(degree)： 只能在下面的方法里面使用
 */
-(NSString *)degree{
    
    return objc_getAssociatedObject(self, _cmd);
}

@end
