//
//  ViewController.m
//  JKCategory2
//
//  Created by 王冲 on 2018/6/30.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "ViewController.h"
#import <objc/runtime.h>
#import "JKPerson.h"
#import "JKPerson+Test.h"
#import "JKPerson+Test2.h"
#import "JKPerson+Test3.h"
#import "JKPerson+Test4.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self test5];
}

#pragma mark 移除关联对象 和 移除所有的关联对象
-(void)test5{
    
    JKPerson *person1 = [JKPerson new];
    person1.degree = @"W";
    
    JKPerson *person2 = [JKPerson new];
    
    // 移除关联对象
    person2.degree = nil;
    
    // 移除所有的关联对象
    objc_removeAssociatedObjects(person2);
    
    NSLog(@"person1 程度==  %@",person1.degree);
    NSLog(@"person2 程度==  %@",person2.degree);
}

#pragma mark 使用属性名作为key :   `#define JKRevelKey @"revel";`
-(void)test4{
    
    JKPerson *person1 = [JKPerson new];
    person1.degree = @"W";
    
    JKPerson *person2 = [JKPerson new];
    person2.degree =@"C";
    
    NSLog(@"person1 程度==  %@",person1.degree);
    NSLog(@"person2 程度==  %@",person2.degree);
    
    
}

#pragma mark 使用get方法的@selecor作为key
-(void)test3{
    
    JKPerson *person1 = [JKPerson new];
    person1.revel = 20;
    
    JKPerson *person2 = [JKPerson new];
    person2.revel = 30;
    
    NSLog(@"person1 水平==  %d",person1.revel);
    NSLog(@"person2 水平==  %d",person2.revel);
}

#pragma mark  static char JKWeightKey; 形式的关联对象
-(void)test2{
    
    JKPerson *person1 = [JKPerson new];
    person1.weight = 40;
    
    JKPerson *person2 = [JKPerson new];
    person2.weight = 20;
    
    NSLog(@"person1 年龄==  %d 身高== %d",person1.age,person1.height);
    NSLog(@"person2 年龄==  %d 身高== %d",person2.age,person2.height);
}

#pragma mark  const void *JKNameKey = &JKNameKey; 形式的关联对象
-(void)test1{
    
    JKPerson *person1 = [JKPerson new];
    person1.age = 20;
    person1.name = @"JK";
    
    JKPerson *person2 = [JKPerson new];
    person2.age = 21;
    person2.name = @"WC";
    
    NSLog(@"person1 年龄==  %d  名字== %@",person1.age,person1.name);
    NSLog(@"person2 年龄==  %d  名字== %@",person2.age,person2.name);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

