//
//  JKPerson.h
//  JKCategory2
//
//  Created by 王冲 on 2018/6/30.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKPerson : NSObject

@property(nonatomic,assign) int age;

@end
