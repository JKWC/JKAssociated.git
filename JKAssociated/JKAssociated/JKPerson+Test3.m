//
//  JKPerson+Test3.m
//  JKAssociated
//
//  Created by 王冲 on 2018/6/30.
//  Copyright © 2018年 JK科技有限公司. All rights reserved.
//

#import "JKPerson+Test3.h"
#import <objc/runtime.h>



#define JKRevelKey @"revel"

@implementation JKPerson (Test3)

-(void)setRevel:(int)revel{
    
    /**
     使用属性名作为key ： @"revel" 的内存地址是一直定值
     */
    objc_setAssociatedObject(self, JKRevelKey, @(revel), OBJC_ASSOCIATION_ASSIGN);
}

-(int)revel{
    
    return  [objc_getAssociatedObject(self, JKRevelKey) intValue];
}

@end
